module app.profile{

 interface IProfileController  {
        call(args:any) : void;
        
    }
    class ProfileController implements IProfileController{
       
        public static $inject = ["$scope","$mdDialog"];
        constructor(public $scope:ng.IScope,public $mdDialog: angular.material.IDialogService) {
            var vm=this.$scope;
                 this.$scope.$on("UpdateUserDetail", function (event, args) {                 
                      vm.$broadcast('UpdateUserCall',args);
                  });     
                   this.$scope.$on("DetailClick", function (event, args) {                 
                      vm.$broadcast('DetailClickCall',args);
                  }); 
                   this.$scope.$on("RefreshUserList", function (event, args) {                 
                      vm.$broadcast('RefreshUserListData',args);
                  }); 
                  
        }
    }
    angular
		.module("app.profile")
		.controller("ProfileController",
			ProfileController);
    
}