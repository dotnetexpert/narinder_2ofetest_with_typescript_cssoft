module app.user {
	export interface IUser {
		userid: number;
		username: string;
		company: string;
		job: string;
		location: string;
		province:string;
		avatar: string;
		type: string;
	}

	export class User implements IUser {

		constructor(public userid: number,
					public username: string,
					public company: string,
					public job: string,
					public location: string,
					public province:string,
					public avatar: string,
					public type: string) {
		}
	}
}