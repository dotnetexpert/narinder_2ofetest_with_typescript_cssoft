namespace app.Service {
	"use strict";

	export class ZmUserRestService {
           public serviceBase:string;
           public users:any;
        static $inject=["$http","localStorageService"];
		constructor(private $httpBackend: ng.IHttpService,private localStorageService:angular.local.storage.ILocalStorageService) {
            this.serviceBase="";
            
		}
        
       public readListOfUser(): ng.IPromise<app.user.IUser[]> {  
           var users = this.localStorageService.get("users");           
              return this.$httpBackend.get('contacts.json').then(response => users);
       }
       
       public readUser(userId:number): ng.IPromise<app.user.IUser> {      
              return  this.$httpBackend.get(this.serviceBase+userId).then(response => response.data);
       }
       
        public deleteUser(userId:number): ng.IPromise<app.user.IUser> {  
              this.users = this.localStorageService.get('users');                
                this.users.splice(userId, 1);
                this.localStorageService.set('users', this.users)   
             return  this.$httpBackend.get('contacts.json').then(response => response);         
              //return  this.$httpBackend.delete(this.serviceBase+userId).then(response => response.data);
       }
       
       public addUser(user:app.user.IUser): ng.IPromise<app.user.IUser> {  
             this.users = this.localStorageService.get('users'); 
             user.userid=this.users.length+1;               
                this.users.unshift(user);
                this.localStorageService.set('users', this.users)    
            return  this.$httpBackend.get('contacts.json').then(response =>response );        
            //   return  this.$httpBackend.post(this.serviceBase,user).then(response => response.data);
       }
       
       public readPersonalDetails(userId:number): ng.IPromise<app.user.IUser> {    
             return  this.$httpBackend.get(this.serviceBase+userId).then(response => response.data);
       }
       
       public updatePersonalDetails(user:app.user.IUser,index:number): ng.IPromise<app.user.IUser> {   
             this.users = this.localStorageService.get('users');             
             this.users.splice(index, 1);               
                this.users.splice(index, 0,user);               
                this.localStorageService.set('users', this.users)           
             return  this.$httpBackend.get('contacts.json').then(response => response);
            //  return  this.$httpBackend.put(this.serviceBase,user).then(response => response.data);
       }
       
       public readCompanyDetails(userId:number): ng.IPromise<app.user.IUser> {             
             // return  this.$httpBackend.get('contacts.json').then(response => users);
             return  this.$httpBackend.get(this.serviceBase+userId).then(response => response.data);
       }
       
       public updateCompanyDetails(user:app.user.IUser,index:number): ng.IPromise<app.user.IUser> {             
            this.users = this.localStorageService.get('users');                
                this.users.splice(index, 0,user);               
                this.localStorageService.set('users', this.users)           
             return this.$httpBackend.get('contacts.json').then(response => response);
             // return  this.$httpBackend.put(this.serviceBase,user).then(response => response.data);
       }
	}

	angular.module("app.profile").service("zmUserRestService",ZmUserRestService);
}