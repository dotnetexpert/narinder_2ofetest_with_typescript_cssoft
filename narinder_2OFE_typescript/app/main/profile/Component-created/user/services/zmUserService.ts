namespace app.Service {
	"use strict";

	export class ZmUserService {

        static $inject=["zmUserRestService"];
		constructor(private zmUserRestService:app.Service.ZmUserRestService) {
		}
        
       public readListOfUser(): ng.IPromise<app.user.IUser[]> {  
               return this.zmUserRestService.readListOfUser().then(response =>response);
            }
            
       public readUser(userId:number): ng.IPromise<app.user.IUser> {             
              return  this.zmUserRestService.readUser(userId).then(response => response);
       }
       
        public deleteUser(userId:number): ng.IPromise<app.user.IUser> {             
              return  this.zmUserRestService.deleteUser(userId).then(response => response);
       }
       
       public addUser(user:app.user.IUser): ng.IPromise<app.user.IUser> {             
              return  this.zmUserRestService.addUser(user).then(response => response);
       }
       
       public readPersonalDetails(userId:number): ng.IPromise<app.user.IUser> {             
              return  this.zmUserRestService.readPersonalDetails(userId).then(response => response);
       }
       
       public updatePersonalDetails(user:app.user.IUser,index:number): ng.IPromise<app.user.IUser> {             
              return  this.zmUserRestService.updatePersonalDetails(user,index).then(response => response);
       }
       
       public readCompanyDetails(userId:number): ng.IPromise<app.user.IUser> {             
              return  this.zmUserRestService.readCompanyDetails(userId).then(response => response);
       }
       
       public updateCompanyDetails(user:app.user.IUser,index:number): ng.IPromise<app.user.IUser> {             
              return  this.zmUserRestService.updateCompanyDetails(user,index).then(response => response);
       }
	}

	angular.module("app.profile").service("zmUserService",ZmUserService);
}