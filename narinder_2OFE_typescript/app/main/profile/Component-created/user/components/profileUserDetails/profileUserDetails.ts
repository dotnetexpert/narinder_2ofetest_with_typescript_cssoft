module app.Components {
 
    interface IZmProfileUserDetailBindings  {
        textBinding: string;
        dataBinding: number;
        functionBinding: () => any;
        
    }
 
    interface IZmProfileUserDetailController extends IZmProfileUserDetailBindings {
        add(): void;
        selected:boolean;
        selectedData:any;
        editContact:boolean;
        delete(ev:any,data:any):void;
        update(ev:any,data:any,index:number):void;
        index:number;
        contact:any;
    }
 
    class ZmProfileUserDetailController implements IZmProfileUserDetailController {
 
        public textBinding:string;
        public dataBinding:number;
        public functionBinding:() => any;
        public selected:boolean;
        public selectedData:any;
        public contact:any;
        public editContact=false;
        public index:number;
        
        public static $inject=["$scope","zmUserService","$mdToast"];        
       
        constructor(private $scope:ng.IScope,private zmUserService:app.Service.ZmUserService, private $mdToast:angular.material.IToastService) {           
            this.textBinding = '';
            this.dataBinding = 0;
            this.selected=false;
            this.editContact=false;
            var vm=this;
            
             this.$scope.$on("UpdateUserCall", function (event, args) {   
                    vm.selected=true;
                    vm.editContact=true;
                    vm.contact=args;
                    vm.selectedData=[];
             });
             
           this.$scope.$on("DetailClickCall", function (event, args) {
                    vm.selected=true;
                    vm.editContact=false;
                    vm.selectedData=args; 
                    vm.contact=[];                  
                    this.index=args.index;                   
                    localStorage.setItem("index",args.index);
          });  
             
        }
 
        add():void {
            this.$scope.$emit("handleEmit","done");
        }
        
        delete(ev,data)
        {
            
            this.zmUserService.deleteUser(localStorage.getItem("index")).then(users => {              
                this.$scope.$emit("RefreshUserList","done");
                this.selected=false;
                this.selectedData=[];
                this.$mdToast.show(
                    this.$mdToast.simple()
                    .textContent("User deleted successfully...")
                    .theme('success-toast')
                    .position('top right')
                 ); 
                }).catch( reason => {
                console.log("something went wrong!");
                });
        }
        update(ev,data)
        {         
             this.selected=false;
             this.editContact=false;
             this.zmUserService.updatePersonalDetails(data,localStorage.getItem("index")).then(users => {              
                this.$scope.$emit("RefreshUserList","done");
                this.$mdToast.show(
                    this.$mdToast.simple()
                    .textContent("User updated successfully...")
                    .theme('success-toast')
                    .position('top right')
                 ); 
                }).catch( reason => {
                console.log("something went wrong!");
                });
        }
       
 
    }
 
    class ZmProfileUserDetail implements ng.IComponentOptions {
 
        public bindings:any;
        public controller:any;
        public templateUrl:string;
 
        constructor() {
            this.bindings = {
                textBinding: '@',
                dataBinding: '<',
                functionBinding: '&'
            };
            this.controller = ZmProfileUserDetailController;
            this.templateUrl = '/app/main/profile/Component-created/user/components/profileUserDetails/zm-profile-user-detail.html';
        }
 
    }
 
    angular.module('app.profile').component('zmProfileUserDetail', new ZmProfileUserDetail());
 
}