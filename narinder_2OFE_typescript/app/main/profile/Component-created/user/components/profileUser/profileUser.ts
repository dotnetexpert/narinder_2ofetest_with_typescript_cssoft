module app.Components {
 
    interface IZmProfileUserBindings  {
        textBinding: string;
        dataBinding: number;
        functionBinding: () => any;
        
    }
 
    interface IZmProfileUserController extends IZmProfileUserBindings {    
        profileList: app.user.IUser[];
        edit(ev:any,profile:any,index):void;
        detail(ev,profile,index):void;
    }
 
    class ZmProfileUserController implements IZmProfileUserController {
 
        public textBinding:string;
        public dataBinding:number;
        public functionBinding:() => any;
        public usersList:app.user.IUser[];
        public gridBind:()=>void;
        public static $inject=["$scope","$mdDialog","zmUserService"];
        constructor(public $scope:ng.IScope,public $mdDialog: angular.material.IDialogService,private zmUserService:app.Service.ZmUserService) {           
            this.textBinding = '';
            this.dataBinding = 0;
            this.usersList=[];
            var vm=this;
            
                this.gridBind=function(){                              
                   zmUserService.readListOfUser().then(users => {                      
                      vm.usersList = users;            
                       }).catch( reason => {
                         console.log("something went wrong!");
                  });
                };
                
                vm.gridBind();
                 
                this.$scope.$on("RefreshUserListData", function (event, args) {  
                   vm.gridBind();
                });  
        }
 
                
        edit(ev,profile,index):void{        
            profile.index=index;
            this.$scope.$emit("UpdateUserDetail",profile);
            event.stopPropagation();
        }
        detail(ev,profile,index):void{
            profile.index=index;
            this.$scope.$emit("DetailClick",profile);
            event.stopPropagation();
        }
        
        AddUserDialog(ev) {
            
        var parentEl = angular.element(document.body);
        
        this.$mdDialog.show({
            parent: parentEl,   // use parent scope in template 
            templateUrl: '/app/main/profile/dialogs/add-user/add-user.html',
            targetEvent: ev,
            clickOutsideToClose: false
        });      
    }        
 
    }
 
    class ZmProfileUser implements ng.IComponentOptions {
 
        public bindings:any;
        public controller:any;
        public templateUrl:string;
 
        constructor() {
            this.bindings = {
                textBinding: '@',
                dataBinding: '<',
                functionBinding: '&'
            };
            this.controller = ZmProfileUserController;
            this.templateUrl = '/app/main/profile/Component-created/user/components/profileUser/zm-profile-user.html';
        }
 
    }
 
    angular.module('app.profile').component('zmProfileUser', new ZmProfileUser());
 
}