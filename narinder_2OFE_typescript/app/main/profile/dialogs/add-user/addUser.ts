module app.AddUser{

 interface IAddUserDialogController  {
        addUser(args:any) : void;
        closeDialog():void;
        
    }
    class AddUserDialogController implements IAddUserDialogController{
       
        public static $inject = ["$rootScope","$mdDialog","zmUserService","$mdToast"];
        
        constructor(public $rootScope:ng.IRootScopeService,public $mdDialog: angular.material.IDialogService,private zmUserService:app.Service.ZmUserService, private $mdToast:angular.material.IToastService) {
            var vm=this.$rootScope;
                   
        }
       addUser(name){
            if(name){
                var user = {
                    "username": name,
                    "userid":0,
                    "company": "Zippel Media",
                    "job": "Developer",
                    "location": "Elsdorf", "province": "North-Rhine-Westfalia",
                    "avatar": "assets/images/avatars/userdefault.png",
                    "type": "user"
                }
                
                this.zmUserService.addUser(user).then(users => {                  
                        this.$rootScope.$broadcast('RefreshUserListData',"");   
                         this.$mdToast.show(
                         this.$mdToast.simple()
                             .textContent("User added successfully...")
                             .theme('success-toast')
                             .position('top right')
                          );        
                }).catch( reason => {
                console.log("something went wrong!");
                });
                        
                this.$mdDialog.hide();
        } 
       }       
    
       closeDialog(){
        this.$mdDialog.hide();
       }
    }
    angular
		.module("app.profile")
		.controller("AddUserDialogController",
			AddUserDialogController);
    
}