var gulp = require('gulp');
var ts = require('gulp-typescript');
var sass = require('gulp-sass');
var browserSync = require('browser-sync').create();
 
 
gulp.task('sass', function() {
  return gulp.src('assets/scss/style.scss') // Gets all files ending with .scss in app/scss and children dirs
    .pipe(sass())
    .pipe(gulp.dest('assets/css/'))
});

gulp.task('build', function () {
	return gulp.src('./app/**/*.ts')
		.pipe(ts({
			noImplicitAny: true,
			out: 'typescripts.js'
		}))
		.pipe(gulp.dest('compiled'));
});

gulp.task('serve', function() {
  browserSync.init({
    server: {
      baseDir: '',     
    },
  })
});